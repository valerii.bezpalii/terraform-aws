resource "aws_instance" "app"  {
  ami = "ami-07dfba995513840b5"
  instance_type = "t2.micro"
  availability_zone = "${var.public_az}"
  key_name = "${var.aws_key_name}"
  vpc_security_group_ids = ["${var.vpc_security_group}"]
  associate_public_ip_address = true
  subnet_id = "${var.vpc-subnet-public}"
  monitoring = false
  tags =  {
    Name = "Nginx_phpfpm_wordpress"

  }
}  
resource "aws_instance" "db-wordpress"  {
  ami = "ami-07dfba995513840b5"
  instance_type = "t2.micro"
  availability_zone = "${var.public_az}"
  key_name = "${var.aws_key_name}"
  vpc_security_group_ids = ["${var.vpc_security_group}"]
  associate_public_ip_address = true
  subnet_id = "${var.vpc-subnet-public}"
  monitoring = false
  
  tags =  {
    Name = "db"

  }
}
  
